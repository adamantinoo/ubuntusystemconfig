#!/bin/bash
# - PARAMETERS & CONSTANTS
BACKUP_DIRECTORY="/home/adam/Data/Backup/Toshiba-Satellite/adam"
RSYNC_CONFIGURATION="--verbose --recursive --backup --update --times --force --links --perms --executability --xattrs --delete-delay --delete-excluded --exclude-from=backup-exclusion.txt --prune-empty-dirs"
cd $HOME
# - Account configuration
echo "> Copying account configuration..."
cp -v .* $BACKUP_DIRECTORY
cp -v * $BACKUP_DIRECTORY

# - Docker services
echo "> Copying Docker services..."
rsync $RSYNC_CONFIGURATION /home/adam/Docker $BACKUP_DIRECTORY

# - Documents
echo "> Copying Documents..."
rsync $RSYNC_CONFIGURATION /home/adam/Documents $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/Downloads $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/Music $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/Pictures $BACKUP_DIRECTORY

# - Documents
echo "> Copying Utilities..."
rsync $RSYNC_CONFIGURATION /home/adam/Utilities $BACKUP_DIRECTORY

# - Applications configurations
echo "> Copying Applications configurations..."
rsync $RSYNC_CONFIGURATION /home/adam/.circleci $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/.config $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/.docker $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/.gitkraken $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/.gnome $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/.m2 $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/.ngrok2 $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/.ssh $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/.vscode $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/.wine $BACKUP_DIRECTORY
rsync $RSYNC_CONFIGURATION /home/adam/.wine32 $BACKUP_DIRECTORY

echo "> Copying Snap configurations..."
rsync $RSYNC_CONFIGURATION /home/adam/snap $BACKUP_DIRECTORY

# - System configuration
echo "> Copying System configuration..."
rsync $RSYNC_CONFIGURATION /etc/fstab $BACKUP_DIRECTORY/system-config
rsync $RSYNC_CONFIGURATION /etc/minidlna.conf $BACKUP_DIRECTORY/system-config


# - Link documentation
# lrwxrwxrwx  1 adam adam    43 feb 23 14:11 .cache -> /home/adam/Data/UbuntuExtension/adam/.cache
# lrwxrwxrwx  1 adam adam    16 may  8  2020 Data -> /mnt/disk-b/data
# lrwxrwxrwx  1 adam adam    44 mar 26 22:07 .gradle -> /home/adam/Data/UbuntuExtension/adam/.gradle
# lrwxrwxrwx  1 adam adam    53 feb  7 16:47 .jeveassets -> /home/adam/Dropbox/EVE/AppConfigurations/.jeveassets/

# - NVM configuration
#adam@Adam-SATELLITE-L70-B:~/Data$ nvm ls
#        v8.17.0
#       v12.22.1
#->     v14.16.1
#default -> lts/fermium (-> v14.16.1)
#gip -> v14.16.1
#innovative -> lts/fermium (-> v14.16.1)
#printer3d -> v14.16.1
#innodental -> lts/carbon (-> v8.17.0)
#node -> stable (-> v14.16.1) (default)
#stable -> 14.16 (-> v14.16.1) (default)
#iojs -> N/A (default)
#unstable -> N/A (default)
#lts/* -> lts/fermium (-> v14.16.1)
#lts/argon -> v4.9.1 (-> N/A)
#lts/boron -> v6.17.1 (-> N/A)
#lts/carbon -> v8.17.0
#lts/dubnium -> v10.24.1 (-> N/A)
#lts/erbium -> v12.22.1
#lts/fermium -> v14.16.1

# - Snap installations
# Name                     Version                     Rev    Tracking         Publisher     Notes
# android-studio           4.1.3.0                     101    latest/stable    snapcrafters  classic
# canonical-livepatch      9.6.1                       98     latest/stable    canonical✓    -
# circleci                 0.1.15085+84d803e           230    latest/stable    circleci✓     -
# code                     3c4e3df9                    62     latest/stable    vscode✓       classic
# core                     16-2.49.2                   10958  latest/stable    canonical✓    core
# core18                   20210309                    1997   latest/stable    canonical✓    base
# core20                   20210319                    975    latest/stable    canonical✓    base
# dbeaver-ce               21.0.3.202104181339         110    latest/stable    dbeaver-corp  -
# discord                  0.0.14                      122    latest/stable    snapcrafters  -
# docker                   19.03.13                    796    latest/stable    canonical✓    -
# eclipse                  2019-12                     48     latest/stable    snapcrafters  classic
# figlet                   2.2.5+git24.202a0a8         36     latest/stable    cmatsuoka     -
# firefox                  88.0-2                      524    latest/stable    mozilla✓      -
# gitkraken                7.5.2                       172    latest/stable    gitkraken✓    classic
# glimpse-editor           0.2.0                       191    latest/stable    trechnex      -
# gnome-3-26-1604          3.26.0.20210401             102    latest/stable/…  canonical✓    -
# gnome-3-28-1804          3.28.0-19-g98f9e67.98f9e67  145    latest/stable    canonical✓    -
# gnome-3-34-1804          0+git.3556cb3               66     latest/stable    canonical✓    -
# gnome-calculator         3.38.2+git3.1d166209        884    latest/stable/…  canonical✓    -
# gnome-characters         v3.34.0+git30.16311a5       708    latest/stable/…  canonical✓    -
# gnome-system-monitor     3.38.0-17-g38c1ce1d62       157    latest/stable/…  canonical✓    -
# gtk-common-themes        0.1-52-gb92ac40             1515   latest/stable/…  canonical✓    -
# gtk2-common-themes       0.1                         13     latest/stable    canonical✓    -
# heroku                   v7.52.0                     4048   latest/stable    heroku✓       classic
# intellij-idea-community  2020.2.4                    265    2020.2/stable    jetbrains✓    classic
# mailspring               1.9.1                       505    latest/stable    foundry376✓   -
# nmap                     7.91                        2280   latest/stable    maxiberta     -
# postman                  7.36.5                      133    latest/stable    postman-inc✓  -
# spotify                  1.1.55.498.gf9a83c60        46     latest/stable    spotify✓      -
# vlc                      3.0.12.1                    2103   latest/stable    videolan✓     -

# - FSTAB
# # /etc/fstab: static file system information.
# #
# # Use 'blkid' to print the universally unique identifier for a
# # device; this may be used with UUID= as a more robust way to name devices
# # that works even if disks are added and removed. See fstab(5).
# #
# # <file system> <mount point>   <type>  <options>       <dump>  <pass>
# # / was on /dev/sdb5 during installation
# UUID=5429065b-3d2d-4269-b63d-1e827bc2d125 /               ext4    errors=remount-ro 0       1
# # /boot/efi was on /dev/sdb2 during installation
# #UUID=5AA5-8DF9  /boot/efi       vfat    umask=0077      0       1
# /swapfile01                                 none            swap    sw              0       0
# /swapfile02                                 none            swap    sw              0       0
# /swapfile03                                 none            swap    sw              0       0
# #/swapfile04                                 none            swap    sw              0       0
# /dev/disk/by-uuid/b340319b-6616-4d81-8600-11a2f9357c56 /mnt/disk-b/data auto nosuid,nodev,nofail,x-gvfs-show 0 0
# UUID=5AA5-8DF9  /boot/efi       vfat    defaults      0       1
# # - Mount the /snapd from another partition
# /mnt/disk-b/data/UbuntuExtension/snapd /var/lib/snapd none bind 0 0
