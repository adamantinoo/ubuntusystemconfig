# - A L I A S E S
alias rm=trash
alias v='ls -la '
alias docker-up='docker-compose -f $(find . -name "docker-compose-yml" | grep -v target) up &'
alias docker-down='docker-compose -f $(find . -name "docker-compose-yml" | grep -v target) down &'
alias proc='ps -elf | grep $1 '
alias backupso='cp /etc/fstab ~/Data/backupso; cp /etc/hosts ~/Data/backupso; cp /etc/sysctl.conf ~/Data/backupso; cp ~/.bash* ~/Data/backupso; cp ~/.netrc ~/Data/backupso; cp -rf /home/adam/.config/Code/User ~/Data/backupso; cp -rf /home/adam/.config/JetBrains/IdeaIC2020.3 ~/Data/backupso'

# - COMMANDS
alias procinfo='ps -elf | grep $1 '
alias removeiml='shopt -s globstar; rm -i **/*.iml'
alias minidlna='sudo service minidlna force-reload; sudo minidlnad -f /etc/minidlna.conf'
alias killportprocess='sudo kill -9 `sudo lsof -t -i:9500`'
alias netstats=' while :; do netstat -na | awk -f ~/Utilities/awkcactiNetStatFilter.awk; sleep 2; done'
alias selectjava='sudo update-alternatives --config java'
function swapusedf() {
    echo -e "\x1b[35m> Swap used all processes: \x1b[1m${1}\x1b[0m"
	for file in /proc/*/status
	do 
		awk '/VmSwap|Name|Tgid/{printf $0 "\t\t"}END{ print ""}' $file
	done
}
function swapusedfb() {
    echo -e "\x1b[35m> Swap used all processes: \x1b[1m${1}\x1b[0m"
	for file in /proc/*/status
	do 
		awk '/^Pid:|VmSwap|Name/{printf $2 " " $3}END{ print ""}' $file
	done
}
alias swapused='swapusedf | sort -r -n -k 6 | head '

# - ANGULAR
alias ng615="~/Utilities/AngularFrameworks/ng6.1.0/node_modules/.bin/ng"
alias ng7216="~/Utilities/AngularFrameworks/ng7.2.16/node_modules/.bin/ng"
alias ng831="~/Utilities/AngularFrameworks/ng831/node_modules/.bin/ng"
alias ng8314="~/Utilities/AngularFrameworks/ng8314/node_modules/.bin/ng"
alias ng8320="~/Utilities/AngularFrameworks/ng8320/node_modules/.bin/ng"
alias ng8322="~/Utilities/AngularFrameworks/ng8322/node_modules/.bin/ng"
alias ng910="~/Utilities/AngularFrameworks/ng910/node_modules/.bin/ng"
alias ng916="~/Utilities/AngularFrameworks/ng916/node_modules/.bin/ng"
alias ng11="~/Utilities/AngularFrameworks/ng1128/node_modules/.bin/ng"
alias ng='ng916 '
alias cypress='./node_modules/.bin/cypress '
alias cucumber='./node_modules/.bin/cucumber-js '

# - BACKEND
function runacctest() {
    echo -e "\x1b[35m> Running Acceptance tests for: \x1b[1m${1}\x1b[0m"
    ./gradlew test -Dcucumber.options="--tags ${1}" --tests **/backend/core/exception/*Test
}

# - TESTING
alias route4200='sudo iptables -t nat -I OUTPUT -p tcp -d 127.0.0.1 --dport 80 -j REDIRECT --to-ports 4200'
alias route8080='sudo iptables -t nat -I OUTPUT -p tcp -d 127.0.0.1 --dport 80 -j REDIRECT --to-ports 8080'

# - BACKUP
alias backupso='cp /etc/fstab ~/Data/backupso; cp /etc/hosts ~/Data/backupso; cp /etc/sysctl.conf ~/Data/backupso; cp ~/.bash* ~/Data/backupso; cp ~/.netrc ~/Data/backupso'

# - DOCKER
alias dockernet='docker network create -d bridge --subnet 192.168.2.0/24 --gateway 192.168.2.1 dockernet'
alias dockerprune='docker image prune; docker network prune; docker container prune; docker volume prune'

# - PYFA
export PYFA_HOME=/home/adam/Dropbox/EVEApplications/Pyfa-2.29.0
alias pyfa="python3 ${PYFA_HOME}/pyfa.py"
alias pyfaup="cd ${PYFA_HOME} && git pull"

# PYTHON
alias kivy='kivy_venv\Scripts\activate '

# WINE
export WINEPREFIX="/home/adam/.wine"
export PICASA3="/home/adam/.wine/dosdevices/c:/Program Files (x86)/Google/Picasa3"

